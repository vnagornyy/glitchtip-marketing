import { Component } from '@angular/core';
import { ScullyRoutesService } from '@scullyio/ng-lib';
import { Observable } from 'rxjs';
import { LinksService } from '../links.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent {
  registerLink = this.links.registerLink;
  links$: Observable<any> = this.scully.available$;

  constructor(
    private scully: ScullyRoutesService,
    private links: LinksService
  ) {}
}
