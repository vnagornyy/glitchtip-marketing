import { AfterViewChecked, Component, ViewEncapsulation } from '@angular/core';
import { ScullyRoutesService } from '@scullyio/ng-lib';
import { map } from 'rxjs/operators';

import { flattenedPlatforms } from 'glitchtip-frontend/src/app/settings/projects/platform-picker/platforms-for-picker';
import { HighlightService } from '../shared/highlight.service';

@Component({
  selector: 'app-sdk-docs',
  templateUrl: './sdk-docs.component.html',
  styleUrls: ['./sdk-docs.component.scss'],
  preserveWhitespaces: true,
  encapsulation: ViewEncapsulation.Emulated,
})
export class SDKDocsComponent implements AfterViewChecked {
  title$ = this.scully.getCurrent().pipe(
    map((currentRoute) => {
      const id = currentRoute.sourceFile?.slice(0, -3);
      return (
        flattenedPlatforms.find((platform) => platform.id === id)?.name || id
      );
    })
  );

  constructor(
    private scully: ScullyRoutesService,
    private highlightService: HighlightService
  ) {}

  ngAfterViewChecked() {
    this.highlightService.highlightAll();
  }
}
